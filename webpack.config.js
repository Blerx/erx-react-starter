const path = require( 'path' );
const HTMLWebpackPlugin = require( 'html-webpack-plugin' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
const webpack = require('webpack');
const bootstrapEntryPoints = require('./webpack.bootstrap.config');
const isProd = process.env.NODE_ENV === 'production';
const bootstrapConfig = isProd ? bootstrapEntryPoints.dev : bootstrapEntryPoints.dev;

module.exports = {
    entry: {
        app: './src/index.js',
        bootstrap: bootstrapConfig
    },
    output: {
        path: path.resolve( __dirname, 'dist' ),
        filename: '[name].js'
    },
    plugins: [
        new HTMLWebpackPlugin( {
            title: 'Erx React and Redux Starter',
            minify: {
                collapseWhitespace: true
            },
            hash: true,
            template: './src/index.ejs'
        } ),
        new ExtractTextPlugin({
            filename: '[name].css',
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract( {
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true, // to check if needed
                                sourceMap: true,
                            },
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ],
                    publicPath: '/dist'
                } )
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: [ 'react', 'es2015', 'stage-1' ]
                }
            },
            { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.(woff2?|svg)$/, loader: 'url-loader?limit=10000' },
            { test: /\.(ttf|eot)$/, loader: 'file-loader' }
        ]
    },
    resolve: {
        modules: ['node_modules'],
        extensions: [ '.jsx', '.scss', '.js', '.json' ]
    },
    devServer: {
        historyApiFallback: true,
        contentBase: path.resolve( __dirname, 'dist' ),
        compress: true,
        stats: 'errors-only',
        open: true
    }
};